
from tkinter import *
from tkinter import filedialog as fd
from PIL import Image, ImageTk, ImageOps
import math
import cv2
from matplotlib import pyplot as plt

outputGCODEfile = 'output.gcode'

root = Tk()
root.title("Harry Plotter Controller")
root.configure(background='white')

# WINDOW SIZING

root.geometry("1024x768")
filename = fd.askopenfilename(initialdir="./images")

canvas_scale = 3
canvas_size = (270 * canvas_scale, 170 * canvas_scale)

x = canvas_size[0]/2
y = canvas_size[1]/2

canvas = Canvas(root, width=canvas_size[0], height=canvas_size[1], bg="white")
canvas.create_rectangle(4, 4, canvas_size[0], canvas_size[1], outline="black", width=2)
canvas.pack(pady=20)

image = Image.open(filename)
image = ImageOps.grayscale(image)
scale = 1.0
image_width = image.width
image_height = image.height
image_pos = (canvas_size[0]/2, canvas_size[1]/2)
resized_image = image.resize((int(image_width * scale),
                             int(image_height * scale)),
                             Image.ANTIALIAS)
img = ImageTk.PhotoImage(resized_image)
my_image = canvas.create_image(image_pos[0], image_pos[1], image=img)

#cv_img = cv2.imread(filename, 0)
#edges = cv2.Canny(cv_img, 10, 200)
# for e in edges:
#     print(e)

# plt.subplot(121),plt.imshow(cv_img,cmap = 'gray')
# plt.title('Original Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(edges,cmap = 'gray')
# plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
# plt.show()

# contours, hierarchy  = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# prev = [0,0]

# for i in contours:
#     count = 0
#     for c in i:    
#         if (count > 0):
#             canvas.create_line(c[0][0], c[0][1], prev[0][0], prev[0][1])
#         prev = c
#         count += 1

#print(contours)
prev = (0,0)
# for line in contours:
#     print("LINE: " + str(line[0]))
#     canvas.create_line(prev[0], prev[1], line[0][0], line[0][1])
#     prev = line[0]
    

lines = []
separation = 6
detail_radius = 2
detail_length = 1


def p2c(r, phi):
    return (r * math.cos(phi), r * math.sin(phi))


def spiral_points():
    global canvas, canvas_size, resized_image, lines, image_pos, separation, detail_radius, detail_length

    # Delete old spiral
    for l in lines:
        canvas.delete(l)
    update_image(False)

    r = detail_length
    b = separation / (2 * math.pi)
    phi = float(r) / b
    count = 0
    h = resized_image.size[1]
    w = resized_image.size[0]
    prevX = canvas_size[0]/2
    prevY = canvas_size[1]/2
    

    dir = detail_radius
    with open(outputGCODEfile, 'w') as f:
        f.write("G1 F20000\n")
        f.write("M106 S255\n")
        f.write("G4 P100\n")
        f.write("M106 S150\n")
        xmid = (canvas_size[0]/2)/canvas_scale
        ymid = (canvas_size[1]/2)/canvas_scale
        f.write("G1 X" + str(xmid) + " Y" + str(ymid)+"\n")
        f.write("M107\n")

        while True:
            count += 1
            x = p2c(r, phi)[0]
            y = p2c(r, phi)[1]
            
            yy = int(h/2 - y)
            xx = int(w/2 - x)

            xdiff = image_pos[0] - canvas_size[0]/2
            ydiff = image_pos[1] - canvas_size[1]/2
            pixelX = xx - xdiff
            pixelY = yy - ydiff
            #print(resized_image.size[0])
            
            if pixelX > 0 and pixelY > 0 and pixelX < resized_image.size[0] and pixelY < resized_image.size[1]:
                pixel_color = 1.0 - (resized_image.getpixel((pixelX, pixelY)) / 255)
            else:
                pixel_color = 0
            #print(pixel_color)
            xMove, yMove = p2c(r + dir * pixel_color, phi)
            yMove = -yMove
            xMove = -xMove
            dir = -dir
            xMove += canvas_size[0]/2
            yMove += canvas_size[1]/2

            #print(xMove, yMove)
            
            if count > 2:
                lines.append(canvas.create_line(prevX, prevY, xMove, yMove))
                f.write("G1 X" + str(round(xMove/canvas_scale, 2)) + " Y" + str(round(yMove/canvas_scale, 2))+"\n")
            prevX = xMove
            prevY = yMove

            phi += float(detail_length) / r 
            r = b * phi

            if r > canvas_size[1] * 0.45:
                break

        f.write("M106 S255\n")
        f.write("G4 P100\n")
        f.write("M106 S150\n")
        f.write("G1 X0 Y0\n")
        f.write("M107\n")




def update_image(showImage=True):
    global img, image_pos, lines, resized_image
    resized_image = image.resize((int(image_width * scale),
                                 int(image_height * scale)),
                                 Image.ANTIALIAS)
    img = ImageTk.PhotoImage(resized_image)
    #print(show_image.get())
    if showImage:
        my_image = canvas.create_image(image_pos[0], image_pos[1], image=img)

    
    #spiral_points()
    

def move(e):
    global image_pos
    image_pos = (e.x, e.y)
    posLabel.config(text="Coordinates: x: " + str(e.x) + "  y: " + str(e.y))
    # Delete old spiral
    for l in lines:
        canvas.delete(l)
    update_image(False)
    update_image()


def scale_image(e):
    global scale, img
    scale = slider_scale.get() / 100
    update_image()

def set_separation(e):
    global separation
    separation = slider_separation.get()
    #update_image()

def set_detail_radius(e):
    global detail_radius
    detail_radius = slider_detail_radius.get()

def set_detail_length(e):
    global detail_length
    detail_length = slider_detail_length.get()

def set_show_image(e):
    pass


# img_label = Label(image=photo)
# img_label.pack()


posLabel = Label(root, text="Coordinates: x: 0  y: 0")
posLabel.pack()
canvas.bind('<B1-Motion>', move)

frame = Frame(root)
l = Label(frame, text="Scale")
slider_scale = Scale(frame, from_=10, to=300, orient=HORIZONTAL)
slider_scale.set(100)
slider_scale.bind("<ButtonRelease-1>", scale_image)
l.pack(side=LEFT)
slider_scale.pack(side=RIGHT)
frame.pack()

frame = Frame(root)
l = Label(frame, text="Separation")
slider_separation = Scale(frame, from_=3, to=100, orient=HORIZONTAL)
slider_separation.set(separation)
slider_separation.bind("<ButtonRelease-1>", set_separation)
l.pack(side=LEFT)
slider_separation.pack(side=RIGHT)
frame.pack()

frame = Frame(root)
l = Label(frame, text="Detail Radius")
slider_detail_radius = Scale(frame, from_=1, to=60, orient=HORIZONTAL)
slider_detail_radius.set(detail_radius)
slider_detail_radius.bind("<ButtonRelease-1>", set_detail_radius)
l.pack(side=LEFT)
slider_detail_radius.pack(side=RIGHT)
frame.pack()

frame = Frame(root)
l = Label(frame, text="Detail Length")
slider_detail_length = Scale(frame, from_=1, to=60, orient=HORIZONTAL)
slider_detail_length.set(detail_length)
slider_detail_length.bind("<ButtonRelease-1>", set_detail_length)
l.pack(side=LEFT)
slider_detail_length.pack(side=RIGHT)
frame.pack()

frame = Frame(root)
button_draw_spiral = Button(frame, text="Draw Spiral", command=spiral_points)
button_draw_spiral.pack(side=RIGHT)

frame.pack()



#spiral_points()

root.mainloop()