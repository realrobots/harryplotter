import serial
import math
import sys, glob
import time
import turtle


class Plotter:

    def __init__(self, portIndex=0, invertPen=False, speed=10):
        """This is the construction class
        portIndex represents the USB port the machine is plugged into. 
        Set to None for no machine, usually 0 otherwise. If it doesn't 
        connect read the list of available ports and find the correct index.

        invertPen: Set to True for light pen, False for pen lifting mechanism

        speed: refers to turtle graphics speed, default 10
        """
        self.send_buffer = []
        self.oks_received = 0
        self.msgs_sent = 0
        self.heading = 0
        self.x = 0
        self.y = 0
        self.invertPen = invertPen
        self.portIndex = portIndex
        
        print("Plotter Initiated")
        if portIndex != None:
            self.connect_serial()

        self.turt = turtle.Turtle()  
        screen = turtle.Screen()
        screen.setworldcoordinates(-1, -1, screen.window_width() - 1, screen.window_height() - 1)
        self.turt.speed(speed)
        self.turt.goto(0, 0)
        self.turt.goto(280*3, 0)
        self.turt.goto(280*3, 170*3)
        self.turt.goto(0, 170*3)
        self.turt.goto(0, 0)
            
    def connect_serial(self):
        print_ports()

        while True:
            if len(serial_ports()) == 0:
                print("No devices connected")
                time.sleep(1)
            else:
                break

        self.ser = serial.Serial(serial_ports()[self.portIndex], 115200, timeout=0.1)
        if self.ser.is_open:
            print("Serial connection successful")
        else:
            print("Connection failed")

    def send(self, message):
        if 'M' in message:
            print(message)
        message = message + '\r\n'
        message = bytes(message, 'utf-8')
        if self.portIndex != None:
            self.ser.write(message)

    def queue(self, message):
        self.send_buffer.append(message)

    def read_file(self, filename):
        with open(filename) as f:
            lines = f.readlines()
            #print(len(lines))

            for i in range(len(lines)):
                if lines[i].startswith("G") or lines[i].startswith("M"):
                    self.queue(lines[i])
                    #print(lines[i])
        

    def set_speed(self, speed):
        """
        Sets the speed (feedrate) of the following moves
        
        Max is about 20000, less than 1000 will be agonizingly slow
        """
        self.queue('G1 F' + str(speed))


    def penup(self):
        """
        Don't draw any following moves, turns off the light pen or lifts the pen mechanism.
        """
        if self.invertPen:
            self.queue('M107')
        else:
            self.queue('M106 S255')
            self.queue('G4 P100')
            self.queue('M106 S150')
        self.turt.penup()
        self.turt.color(1, 0, 0)

    def pendown(self):
        """
        Draw for following moves, turns on the light pen or lowers the pen mechanism.
        """
        if self.invertPen:
            self.queue('M106')
        else:
            self.queue('M107')
        self.turt.pendown()
        self.turt.color(0, 0, 0)

    def goto(self, x, y):
        """
        Directly move from the current position to the given cartesian (x, y) coordinates.
        """
        #self.turt.goto(x, y)
        self.x = x
        self.y = y
        self.queue('G1 X' + str(round(x, 2)) + ' Y' + str(round(y, 2)))

    def draw_square(self, x, y, len):
        """
        Draw a square with the bottom left corner being the x,y coordinates.
        """
        self.penup()
        self.goto(-len+x,-len+y)
        self.pendown()
        self.goto(-len+x,len+y)
        self.goto(len+x,len+y)
        self.goto(len+x,-len+y)
        self.goto(-len+x,-len+y)

        
    def draw_polygon(self, posX, posY, radius, sides):
        """
        Draw an equilateral polygon at the given x, y coordinates, with the radius and number of sides given.
        """
        count = 0
        for i in range(0, 361, int(360/sides)):
            count = count + 1
            if count == 2:
                self.pendown()
            
            tgt = polar_to_cart(i, radius)
            self.goto(posX + tgt[0], posY + tgt[1])


    def drawgcode(self, cmd):
        """
        Takes a line of GCODE and interprets it as a turtle command
        """
        if cmd.startswith('G'): #G1 X10 Y20
            if len(cmd.split()) < 3:
                return
            
            x = cmd.split(' ')[1]
            y = cmd.split(' ')[2]
            x = x.replace('X', '')
            y = y.replace('Y', '')
            y = y.replace('\n', '')
            x = float(x)
            y = float(y)
            x = x*3# - 280
            y = y*3# - 170
            self.turt.goto(x, y)
        elif cmd.startswith('M'):
            if 'M106' in cmd: # Pen Up
                self.turt.color(1, 0.8, 0)
            elif 'M107' in cmd: # Pen Down
                self.turt.color(0, 0, 0)

    
    def is_penup_down_command(self, cmd):
        """
        Tests if a GCODE command is a penup or pendown command.
        """
        return cmd.startswith('M')


    def invert_penup_down_command(self, cmd):
        """
        Tests if a GCODE command is a penup or pendown and returns it inverted.
        """
        if 'M106' in cmd: # Pen Up
            return 'M107'
        elif 'M107' in cmd: # Pen Down
            return 'M106'
        else:
            return cmd


    def read_incoming(self):
        """
        Reads incoming serial messages and checks for 'ok' from plotter.
        """
        if self.portIndex != None:
            line = self.ser.readline()
            #print(line)
            if line == b'ok\n':
                self.oks_received += 1
                print('rcvd: ok')

    def send_next(self):
        """
        Sends the next message in the send buffer.
        """
        if self.portIndex != None:
            if len(self.send_buffer) == 0:
                return
        print(self.oks_received, self.msgs_sent)

        if self.invertPen:
            if self.is_penup_down_command(self.send_buffer[0]):                
                self.send_buffer[0] = self.invert_penup_down_command(self.send_buffer[0])


        if self.oks_received >= self.msgs_sent or self.portIndex == None:
            self.drawgcode(self.send_buffer[0])
            self.send(self.send_buffer[0])
            self.send_buffer.pop(0)
            self.msgs_sent += 1
        


    def done(self):
        """
        An infinite loop mirroring the turtle.done() command. This loop begins sending
        moves to the plotter and listening for replies.

        This should be the last line of code AFTER all moves are queued as it will block
        any further code from running.
        """
        while True:
            self.read_incoming()

            self.send_next()

            if len(self.send_buffer) == 0:
                break

        turtle.done()


def polar_to_cart(angle, radius):
        """
        Takes polar (angle,radius) coordinates and converts them into cartesian (x,y).
        """
        angle=angle*math.pi/180
        x=radius*math.cos(angle)
        y=radius*math.sin(angle)
        return (round(x, 2), round(y, 2))


def serial_ports():
    """
    Lists the available serial ports.
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def print_ports():
    """
    Prints the available serial ports, with index for each.
    """
    for i in range(0, len(serial_ports())):
        print(str(i) + ": " + serial_ports()[i])
