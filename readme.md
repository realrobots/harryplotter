First you need to download all the files as a .zip
Then unzip the folder and move it to Documents.

From there you should run VSCode (or your favourite IDE) and open that folder

Open the terminal with CTRL-SHIFT-` or VIEW->TERMINAL

You need to have installed Python 3

To install all required python libraries type the following commend
`pip install -r requirements.txt`
or if that fails
`pip3 install -r requirements.txt`


**Draw a Spiral Image**

Now you can generate a spiral image by running `drawspiral.py`

`python3 drawspiral.py`

Choose the image that you want, play with settings until you're happy. Whenever you generate a spiral it automatically gets written to output.gcode.


**Trace an Image**

To trace an image, best for cartoony images, run `traceimage.py`

`python3 traceimage.py`

Choose an image and play with the settings until you're happy, to generate gcode press the gcode tab at the top, generate the gcode and save it. It will replace the current output.gcode.


**Send GCODE to Harry**

To actually draw your gcode file you have to send it to the plotter, run `sendtoharry.py`.

If it fails and spits out a bunch of `0 1` messages then it didn't find the connected plotter, or else is connecting to the wrong device. Read the list of devices printed before the `0 1` list and then take the index of the correct one and put it in line 4 of `sendtoharry.py`.

eg. 
`plotter = harryplotter.Plotter(portIndex=0, invertPen=False)  <----- change portIndex=0 to the correct number`


**Drawing with the light pen**

In order to use the light pen the penUp and penDown commands have to be inverted, since the pen lifts up off the page when powered while the light works the opposite way and turns on and draws when powered. 
The quick way to fix this when reading a GCODE file is to change the 'invertPen=False' to 'invertPen=True', this will flip any command to turn the pen power ON to OFF instead, and vice versa.